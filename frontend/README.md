## Available Scripts

In the **backend** and **frontend** directory we can run to start the project,

### `npm start` or `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Use Tools and technology

#### Reactjs - used for development of the front-end user interfaces or UI components,
#### Expressjs - used for development of the back end APIs,
#### MongoDB - as a database for the project,
#### Bootstrap - a CSS framework to style the UI components,
#### Braintree - Online Payment Solutions and Global Payment Processor

