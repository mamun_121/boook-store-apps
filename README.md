## Available Scripts

In the **backend** and **frontend** directory we can run to start the project,

### `npm start` or `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Use Tools and technology

#### Reactjs - used for development of the front-end user interfaces or UI components,
#### Expressjs - used for development of the back end APIs,
#### MongoDB - as a database for the project,
#### Bootstrap - a CSS framework to style the UI components,
#### Braintree - Online Payment Solutions and Global Payment Processor


# Functionality of the Apps:

### Home Page
All the available books will show in this page. It is also divided into two category, New Arrival which are added recently, and Best Seller based on number of the books sell.

![Home Pge](images/home-page.png)

### Shopping Page
All the available books will show in this page. It has also searched functionality based on the Category and Price range, therefore it will be easy to find particular group books within certain price range.

![Shopping Pge](images/shop-page.png)


### Signup Page
A core functionality of the books' app is signup page also known as registration page enables users and organizations to independently register and gain access to the system. It is based on the Name, Email, and Password and validate each field using simple regular expression.

![Signup Pge](images/signup.png)



### SignIn Page
Based on the user credentials email ans password a user can log in into the system. The Email and Password are validated using simple regular expression.

![SignIn Pge](images/signin.png)

### Admin Dashboard Page
There are two types of user here in this apps, normal user and admin user.
An admin can manage all the functionality of the CRUD operation. It can add a
new category of the product, add a new product for the apps. It can also manage all the
order from the customers and mangege the products, update an existing product or delete a product.

![Admin Pge](images/admin-page.png)

### Add New Category Page
This is a functionality for the Admin, if it's want, it can create a new category books for the
apps store. After adding a new category it will also add total category dropdown menu.

![Category Pge](images/category.png)


### Add New Product Page
An admin also can add a new products to the home page. It will contain all the details of the
books, for example omage of the book, Title, Description, price, category, shipping, quantile.

![Product Pge](images/product.png)

### Manage Order Page
If the orders are added by clients, they can visible on the order page, and the admin can manage the order. It is also possible to change the status of the order, there are a few statuses we can set for example Not Processing, Processed, Shipped, Delivered, Cancelled.

![Order Pge](images/manage-order.png)

### Manage Product Page
An admin can manage the product page, it can update the details of a created products or delete
the existing products.
![Product Pge](images/manage-product.png)

### Shopping Page
After adding a product to the cart, it is also possible to buy this product. There are two options are available here, using the credit card and PayPal both.
![Product Pge](images/shopping cart.png)


### User Profile Page
After creating a new user, the User can also manage and update its profile. It can update its Name, Email, and Password. It can also browse all the purchased products history.
![User Profile Pge](images/user-profile.png)
